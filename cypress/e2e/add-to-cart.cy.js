/* Creator: Joseph ocero
 * @LastEditors: 
 * @DateCreated: 2022-07-28
 * @LastEditTime: 
 * @Description: Select exclusive food to eat
 **/
/// <reference types="cypress" />
import Faker from 'faker';

describe("Central PH - food to eat", function() { 
    const data_ids = [
        34846,
        33849,
        34844,
        33851,
        34850,
        33852,
    ]; 
    
    beforeEach(() => {
        // Go to eats menu and scroll down a bit to see the exclusing for food to eat
        cy.visit('/eats#!');
         cy.get("div[id='root']")
            .scrollTo('0%', '35%')
    })
    afterEach(() => {
        // Remove any cart data after the test
        cy.get("#headerContainer > li.nav-item.header-cart > a > span").click()
        cy.get("#cart-outer-container-2 > div.srnt-cart-per-brand > div > div.srnt-cart-content > div.srnt-cart-items > div > div.srnt-cart-item-controls > div.srnt-cart-item-quantity-controls > a.btn.srnt-cart-item-qty-btn.srnt-cart-item-decrement > svg")
            .click()
            .then(() => {
                cy.get("body > div.swal2-container.swal2-center.swal2-backdrop-show > div > div.swal2-actions > button.swal2-confirm.srn-cart-remove-cart-item-confirm.swal2-styled")
                        .click()
                })

    })
  
    it("Verify the Customer can select any menu from exclusive food to eat via pickup",{scrollBehavior: false }, () => { 

        var random = data_ids[(Math.random() * data_ids.length) | 0]
        const indexValRandom = data_ids.indexOf(random) + 1;
         
        cy.get(`#srn-product-tags-list-double\\ treats\\ eats > div:nth-child(${indexValRandom}) > div.srn-product-tags-details > div.srn-product-tags-price > span`)
            .invoke('text')
            .then((price) => {
                Cypress.env('price', price)
            });
        cy.get(`a[data-cy='eats-landing-parent-${random}']`)
            .trigger('mouseover')
            .click()
            .then(() => {
                cy.get("#srn-fulfillmenttype-container > div > div.srn-ft-fulfillment-options > p:nth-child(2) > span")
                    .click()
                cy.get("select[name='pickup_city']")
                    .select('CALOOCAN CITY')

                cy.get("#srn-fulfillmenttype-dropdown-store").select("SM Grand Central")
                cy.get("#Pickup > button")
                .click()
                .then(() => {
                    cy.get("#headerContainer > li.nav-item.header-cart > a > span").click()
                    
                    cy.get("#cart-outer-container-2 > div.srnt-cart-per-brand > div > div.srnt-cart-content > div.srnt-cart-items > div > div.srnt-cart-item-content-container > div.srnt-cart-item-content > p.srnt-cart-item-price.r-price")
                    .invoke('text')
                    .then((cartPrice) => {
                        
                        expect(cartPrice).to.be.equal(Cypress.env('price'))
                    });
                })
            })
    })
})
   


